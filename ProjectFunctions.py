from numpy import arctan,pi,cos,sin,array,sqrt,cosh
from numpy.linalg import inv
from numpy import linalg
import numpy as np
from COBE import *
from scipy.optimize import minimize

def PrepareEvent(events): # Inputs the array of (eta,phi) (98,64) arrays and outputs an array of 100 (14,8) arrays of Et (divides by cosh(eta)). 
    lessevents = array([events[i] for i in range(100)])
    cutevents = array([array(partition2D(event,8,7)).sum(axis=(2,3)) for event in lessevents])
    etal = np.linspace(-4.9,4.9,14)    
    for event in cutevents: 
        for j in range(14): 
            for i in range(8): 
                event[j][i] = event[j][i]/cosh(etal[j])
    return cutevents
            
def GetEtThresh(event1): # Returns ET Threshold calculated in part 1
    arrayeta = 14
    arrayphi = 8

    N = arrayphi*arrayeta
    Etl = event1.flatten()
    Etl = np.sort(Etl)

    Nremove = round(N/20) # Choose 5 % of events and create new arrays cutting off the front and back after they are sorted
    Etl = Etl[Nremove:]
    Etl = Etl[:-Nremove]

    avgEt = (1/N)*sum(Etl) # Trimmed Average Calculation
    
    Etl = event1.flatten()
    Etl = np.sort(Etl)
    Etlhighlow = Etl[:-(N - Nremove)] # Array containing the bottom 5 percent of Et events
    Etllow = Etl[:-Nremove] # Array containing the bottom 95 percent of et events.
    Vpatch = (1/N)*(sum( (Etllow - avgEt)**2 ) + sum( (Etlhighlow - avgEt)**2 )) # Formula for variance from part 2
    return avgEt + 5*sqrt(Vpatch)

def GetVariance(event1): # Returns variance calculated in part 1 (some redundancy I know)
    arrayeta = 14
    arrayphi = 8    
    N = arrayeta*arrayphi
    Etl = event1.flatten()
    Etl = np.sort(Etl)

    Nremove = round(N/20)
    Etl = Etl[Nremove:]
    Etl = Etl[:-Nremove]

    avgEt = (1/N)*sum(Etl)
    
    Etl = event1.flatten()
    Etl = np.sort(Etl)
    Etlhighlow = Etl[:-(N - Nremove)]
    Etllow = Etl[:-Nremove]
    Vpatch = (1/N)*(sum( (Etllow - avgEt)**2 ) + sum( (Etlhighlow - avgEt)**2 )) 
    return Vpatch


def GetMET(event1): # Returns the Algebraically calculated missing ET from a 14 by 8 eta by phi array, area .8 x .7
    arrayeta = 14
    arrayphi = 8
    Vpatch = GetVariance(event1)
    EtThresh = GetEtThresh(event1)
    phi = np.linspace(-pi,pi,arrayphi) 

    # We now set up two arrays for the sin and cosine of the angular position to correspond to the Et in the same index of event1
    
    cosphi = cos(phi)
    sinphi = sin(phi) # SEAN'S EYES ONLY: This may be assigning the incorrect angles. 
    for j in range(arrayeta - 1): # One less time since cosphi is already filled with one copy 
        for i in range(arrayphi): 
            cosphi = np.append(cosphi,cosphi[i])

    for j in range(arrayeta - 1): # One less time since sinphi is already filled with one copy 
        for i in range(arrayphi): 
            sinphi = np.append(sinphi,sinphi[i])




    # Now we need an Ex list and an Ey list. ETL is two dimensional so we need to collapse it in a reasonable way. 

    Etl = event1.flatten()
    Ex = Etl*cosphi
    Ey = Etl*sinphi

    # We need to sort the events into low scattering and high scattering. We do this by appending to two arrays and the same with the cosine and sine to preserve the angle ordering. 
    
    EtLP = array([])
    EtHP = array([])
    cosphiLP = array([])
    sinphiLP = array([])
    cosphiHP = array([])
    sinphiHP = array([])
    phiHP = array([])
    for i in range(len(Etl)): 
        if Etl[i] < EtThresh: 
            EtLP = np.append(EtLP,Etl[i])
            cosphiLP = np.append(cosphiLP,cosphi[i])        
            sinphiLP = np.append(sinphiLP,sinphi[i])
        else: 
            EtHP = np.append(EtHP,Etl[i])
            cosphiHP = np.append(cosphiHP,cosphi[i])        
            sinphiHP = np.append(sinphiHP,sinphi[i])

# Events with no high scattering are discarded and the MET is set to zero.
            
    if len(EtHP) == 0: 
        return 0.0
            

    # Calculation of the uncertainty based on suggestions from the paper. 
    
    
    Nenergies = arrayeta*arrayphi
    sigma = np.zeros( Nenergies)
    r0 = .05
    r = .5
    for i in range(Nenergies): 
            sigma[i] = sqrt(r0**2 + r**2*Etl[i])

    # X dependents, Vpatch has been defined.

    def delta(i,k):  # Simple delta function
        if i == k: 
            return 1
        else: 
            return 0

    def V(i,j): # Returns the covariance matrix scalar V_ij
        return sum( sigma**2 * cosphi**(4 - (i+j)) * sinphi**( i+j - 2 )) 

    Vcov = array([ [V(1,1),V(1,2)],[ V(2,1),V(2,2) ] ])
    
    
    
    
    # ci dependents. 
    # Ex and Ey are calculated in the usual manner

    ExLP = EtLP*cosphiLP
    EyLP = EtLP*sinphiLP

    # Alow is the total area of the low patches and A is an array containing the area of each high patch (constant arrays filled with .56)
    
    A = np.zeros( len(EtHP) )
    Alowl = np.zeros( len(EtLP) )
    A = array([.8*.7 for i in A])
    Alowl = array([.8*.7 for i in Alowl]) # I am using phi times eta of the area, idk if thats good
    Alow = sum(Alowl)

    
    
    
    # Definitions for the fit parameter epsT calculation
    
    s = 1
    m = len(EtHP) # m is the number of high patches.
    Vcovinv = inv(Vcov)
    def Xmat(i,k): # Returns X_ik for the matrix X. An awkward calculation but easier to view on paper.  
        s = 1
        return array([cosphiHP[i],sinphiHP[i]])@Vcovinv@array([cosphiHP[k],sinphiHP[k]]) + delta(i,k)/(s*Vpatch) # Maybe need a special function to multiply these matrices, also apply transform to the second vector
    X = array([[Xmat(i,k) for k in range(m)] for i in range(m)]) # X i mxm Also the function might return a 1x1 array


    def c(i): # Returns the parameter c_i used to calculate epsilon_i
        return (A[i]/Alow)*( sum(EtLP)/(s*Vpatch) ) - array([cosphiLP[i],sinphiLP[i]]) @ Vcovinv @ array([sum(ExLP),sum(EyLP)]) # Sum is from j = 1 to Nlow in all cases
    c = array([c(i) for i in range(len(EtHP))])

    
    
    
    
    # Fit parameter time ;)
    
    epsT = inv(X) @ c # X is mxm
    print("Fit parameters: ", epsT)
    
    # High patch Ex time ;)
    
    ExHP = EtHP*cosphiHP
    EyHP = EtHP*sinphiHP
    
    Exmiss = - sum( ExHP - epsT*cosphiHP ) # Sum from 1 to m, where m is the number of high et patches
    Eymiss = - sum( EyHP - epsT*sinphiHP ) 
    
    MET = sqrt(Exmiss**2 + Eymiss**2)
    
    
    
    
    
    def check(k): # A little function I made to check the accuracy of the epsilon vector. Should equal zero. 
        
        check = array([ cosphiHP[k],sinphiHP[k]] ) @ Vcovinv @ array( [ sum(ExLP) + sum(epsT*cosphiHP),sum(EyLP) + sum(epsT*sinphiHP)] ) - 1/Vpatch*((A[k]/Alow * sum(EtLP)) - epsT[k])
        return check
    
    return MET




def GetMETN(event1): 
    arrayeta = 14
    arrayphi = 8
    Vpatch = GetVariance(event1)
    EtThresh = GetEtThresh(event1)
    phi = np.linspace(-pi,pi,arrayphi) 

    # We now set up two arrays for the sin and cosine of the angular position to correspond to the Et in the same index of event1

    cosphi = cos(phi)
    sinphi = sin(phi) # SEAN'S EYES ONLY: This may be assigning the incorrect angles. 
    for j in range(arrayeta - 1): # One less time since cosphi is already filled with one copy 
        for i in range(arrayphi): 
            cosphi = np.append(cosphi,cosphi[i])

    for j in range(arrayeta - 1): # One less time since sinphi is already filled with one copy 
        for i in range(arrayphi): 
            sinphi = np.append(sinphi,sinphi[i])




    # Now we need an Ex list and an Ey list. ETL is two dimensional so we need to collapse it in a reasonable way. 

    Etl = event1.flatten()
    Ex = Etl*cosphi
    Ey = Etl*sinphi

    # We need to sort the events into low scattering and high scattering. We do this by appending to two arrays and the same with the cosine and sine to preserve the angle ordering. 

    EtLP = array([])
    EtHP = array([])
    cosphiLP = array([])
    sinphiLP = array([])
    cosphiHP = array([])
    sinphiHP = array([])
    phiHP = array([])
    for i in range(len(Etl)): 
        if Etl[i] < EtThresh: 
            EtLP = np.append(EtLP,Etl[i])
            cosphiLP = np.append(cosphiLP,cosphi[i])        
            sinphiLP = np.append(sinphiLP,sinphi[i])
        else: 
            EtHP = np.append(EtHP,Etl[i])
            cosphiHP = np.append(cosphiHP,cosphi[i])        
            sinphiHP = np.append(sinphiHP,sinphi[i])

    if len(EtHP) == 0: 
        return 0
    # Events with no high scattering are discarded and the MET is set to zero.

    ExLP = EtLP*cosphiLP
    EyLP = EtLP*sinphiLP

    A = np.zeros( len(EtHP) )
    Alowl = np.zeros( len(EtLP) )
    A = array([.8*.7 for i in A])
    Alowl = array([.8*.7 for i in Alowl]) # I am using phi times eta of the area, idk if thats good
    Alow = sum(Alowl)
    def V(i,j): # Returns the covariance matrix scalar V_ij
        return sum( sigma**2 * cosphi**(4 - (i+j)) * sinphi**( i+j - 2 )) 

    sigma = np.zeros( 14*8)
    r0 = .05
    r = .5
    for i in range(14*8): 
            sigma[i] = sqrt(r0**2 + r**2*Etl[i])
            
    guess = np.zeros(len(EtHP))
    def chis(epsl): 
        delta = np.zeros(len(epsl)+2)
        delta[0] = sum(ExLP) + sum(epsl*cosphiHP)
        delta[1] = sum(EyLP) + sum(epsl*sinphiHP)
        for i in range(len(epsl)): 
            delta[i+2] = (A[i]/Alow)*sum(EtLP) - epsl[i]
        Vmat = np.zeros((len(epsl)+2,len(epsl)+2))
        Vmat[0][0] = V(1,1)
        Vmat[1][0] = V(2,1)
        Vmat[0][1] = V(1,2)
        Vmat[1][1] = V(2,2)
        for i in range(len(epsl)): 
            Vmat[i+2][i+2] = Vpatch
        return delta @ inv(Vmat) @ delta
    epsT = minimize(chis,guess).x
    print("Fit parameters: ", epsT)
    ExHP = EtHP*cosphiHP
    EyHP = EtHP*sinphiHP    
    Exmiss = - sum( ExHP - epsT*cosphiHP ) # Sum from 1 to m, where m is the number of high et patches
    Eymiss = - sum( EyHP - epsT*sinphiHP ) 
    MET = sqrt(Exmiss**2 + Eymiss**2)
    return MET