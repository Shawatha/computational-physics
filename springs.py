from numpy import zeros,empty
from pylab import plot,show

N = 26
C =1.0
m = 1.0
k = 6.0
omega = 2.0
alpha = 2*k-m*omega*omega

A = zeroes([N,N],float)
for I in range(N-1): 
    A[i,i] = alpha
    A[i,i+1] = -k
    A[i+1,i] = -k

A[0,0] = alpha - k
A[N-1,N-1] = alpha - k
v = zeroes(N,float)
v[0] = C

for i in range(N-1): 
    
    A[i,i+1] /= A[i,i]
    v[i] /= A[i,i]
    
    A[i+1,i+1] -= A[i+1,i]*A[i,i+1]
    v[i+1] -= A[i+1,i]*v[i]

v[N-1] /= A[N-1,N-1]

x = empty(N,float)
x[N-1] = v[N-1]
for i in range(N-2,-1,-1): 
    x[i] = v[i] - A[i,i+1]*x[i+1]

plot(x)
plot(x,"ko")
show()
