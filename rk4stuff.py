def rk4(x,t,tau,derivsRK,*param): 
        k1 = tau*derivsRK(x,t,*param)
        k2 = tau*derivsRK(x+0.5*k1,t+0.5*tau,*param)
        k3 = tau*derivsRK(x+0.5*k2,t+0.5*tau,*param)
        k4 = tau*derivsRK(x+k3,t+tau,*param)
        xout = x + (k1+2*k2+2*k3+k4)/6
        return xout